namespace StrategyVisitor.Visitors
{
    public class Params
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string CurrentNode { get; set; }
        public string ParentNode { get; set; }
        public EnumTypeObject EnumTypeObject { get; set; }
    }

    public enum EnumTypeObject
    {
        ObjectType = 0,
        Elementary = 1
    }
}