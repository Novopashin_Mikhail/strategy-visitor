using System.Collections.Generic;
using StrategyVisitor.Component;
using StrategyVisitor.Serializers;

namespace StrategyVisitor.Visitors
{
    public class Visitor
    {
        private StrategySerializer _serializer;

        public Visitor(StrategySerializer serializer)
        {
            _serializer = serializer;
        }

        public string Visit(MyPoint point)
        {
            var list = new List<Params>
            {
                new Params {Name = nameof(MyPoint), Value = null, CurrentNode = "root", ParentNode = null, EnumTypeObject = EnumTypeObject.ObjectType},
                new Params {Name = nameof(point.X), Value = point.X.ToString(), ParentNode = "root", EnumTypeObject = EnumTypeObject.Elementary},
                new Params {Name = nameof(point.Y), Value = point.Y.ToString(), ParentNode = "root", EnumTypeObject = EnumTypeObject.Elementary}
            };
            return _serializer.Serialize(list);
        }
        
        public string Visit(Square square)
        {
            var list = new List<Params>
            {
                new Params {Name = nameof(Square), Value = null, CurrentNode = "root", ParentNode = null, EnumTypeObject = EnumTypeObject.ObjectType},
                new Params {Name = nameof(square.Side), Value = square.Side.ToString(), ParentNode = "root", EnumTypeObject = EnumTypeObject.Elementary},
            };
            return _serializer.Serialize(list);
        }
        
        public string Visit(Circle circle)
        {
            var list = new List<Params>
            {
                new Params {Name = nameof(Circle), Value = null, CurrentNode = "root", ParentNode = null, EnumTypeObject = EnumTypeObject.ObjectType},
                new Params {Name = nameof(circle.Radius), Value = circle.Radius.ToString(), ParentNode = "root", EnumTypeObject = EnumTypeObject.Elementary},
            };
            return _serializer.Serialize(list);
        }
    }
}