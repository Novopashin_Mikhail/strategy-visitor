using System;
using StrategyVisitor.Visitors;

namespace StrategyVisitor.Component
{
    public class Circle : IShape
    {
        public int Radius { get; set; }
        
        public void Accept(Visitor visitor)
        {
            var str = visitor.Visit(this);
            Console.WriteLine(str);
        }
    }
}