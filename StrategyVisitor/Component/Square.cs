using System;
using StrategyVisitor.Visitors;

namespace StrategyVisitor.Component
{
    public class Square : IShape
    {
        public int Side { get; set; }
        
        public void Accept(Visitor visitor)
        {
            var str = visitor.Visit(this);
            Console.WriteLine(str);
        }
    }
}