using StrategyVisitor.Visitors;

namespace StrategyVisitor.Component
{
    public interface IShape
    {
        void Accept(Visitor visitor);
    }
}