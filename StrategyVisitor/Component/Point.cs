using System;
using StrategyVisitor.Visitors;

namespace StrategyVisitor.Component
{
    public class MyPoint: IShape
    {
        public int X { get; set; }
        public int Y { get; set; }
        
        public void Accept(Visitor visitor)
        {
            var str = visitor.Visit(this);
            Console.WriteLine(str);
        }
    }
}