using System.Collections.Generic;
using System.Linq;
using System.Text;
using StrategyVisitor.Visitors;

namespace StrategyVisitor.Serializers
{
    public class XmlSerializer : StrategySerializer
    {
        public override string Serialize(IEnumerable<Params> obj)
        {
            var sb = new StringBuilder();
            var root = obj.FirstOrDefault(x => x.CurrentNode == "root");

            sb.Append($"<{root.Name}>");
            foreach (var node in obj.Where(x => x.ParentNode == root.CurrentNode))
            {
                sb.Append($"<{node.Name}>{node.Value}</{node.Name}>");
            }
            sb.Append($"</{root.Name}>");

            return sb.ToString();
        }
    }
}