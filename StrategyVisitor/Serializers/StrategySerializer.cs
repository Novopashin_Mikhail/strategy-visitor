using System.Collections.Generic;
using StrategyVisitor.Visitors;

namespace StrategyVisitor.Serializers
{
    public abstract class StrategySerializer
    {
        public abstract string Serialize(IEnumerable<Params> obj);
    }
}