using System.Collections.Generic;
using System.Linq;
using System.Text;
using StrategyVisitor.Visitors;

namespace StrategyVisitor.Serializers
{
    public class JsonSerializer : StrategySerializer
    {
        public override string Serialize(IEnumerable<Params> obj)
        {
            var sb = new StringBuilder();
            var root = obj.FirstOrDefault(x => x.CurrentNode == "root");
            if (root == null) return null;

            if (root.EnumTypeObject == EnumTypeObject.ObjectType)
            {
                sb.Append($"{{{root.Name}:{{");
            }
            else
            {
                sb.Append($"{{{root.Name}:{root.Value}}}");
                return sb.ToString();
            }

            foreach (var node in obj.Where(x => x.ParentNode == root.CurrentNode))
            {
                if (node.EnumTypeObject == EnumTypeObject.ObjectType)
                {
                    sb.Append($"{node.Name}:{{");
                }
                else
                {
                    sb.Append($"{node.Name}:{node.Value},");
                }
            }

            sb.Append($"}}}}");
            var result = sb.ToString().Replace(",}", "}");
            return result;
        }
    }
}