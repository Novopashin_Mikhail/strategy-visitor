﻿using System;
using System.Drawing;
using StrategyVisitor.Component;
using StrategyVisitor.Serializers;
using StrategyVisitor.Visitors;

namespace StrategyVisitor
{
    class Program
    {
        static void Main(string[] args)
        {
            var jsonSerializer = new JsonSerializer();
            var jsonVisitor = new Visitor(jsonSerializer);
            
            var xmlSerializer = new XmlSerializer();
            var xmlVisitor = new Visitor(xmlSerializer);
            
            var point = new MyPoint{X = 10, Y = 20};
            var circle = new Circle{Radius = 300};
            var square = new Square{Side = 500};
            
            point.Accept(jsonVisitor);
            circle.Accept(jsonVisitor);
            square.Accept(jsonVisitor);
            Console.WriteLine("-------------------------------------");
            
            point.Accept (xmlVisitor);
            circle.Accept(xmlVisitor);
            square.Accept(xmlVisitor);
        }
    }
}